import axios from "axios";

export const Valuet = () => {
  const currency = {usd: {}, rub: {}, eur: {}};
  axios({
    method: "GET",
    url: `https://v6.exchangerate-api.com/v6/cfb6d4cb0ddf0ee3419336f1/latest/USD`,
  })
    .then((response) => {
      currency.usd = response.data.conversion_rates;
    })
    .catch((error) => {
      console.log("axios error:", error);
    });
  axios({
    method: "GET",
    url: `https://v6.exchangerate-api.com/v6/cfb6d4cb0ddf0ee3419336f1/latest/RUB`,
  })
    .then((response) => {
      currency.rub = response.data.conversion_rates;
    })
    .catch((error) => {
      console.log("axios error:", error);
    });
  axios({
    method: "GET",
    url: `https://v6.exchangerate-api.com/v6/cfb6d4cb0ddf0ee3419336f1/latest/EUR`,
  })
    .then((response) => {
      currency.eur = response.data.conversion_rates;
    })
    .catch((error) => {
      console.log("axios error:", error);
    });

  if (currency.usd && currency.rub && currency.eur) {
    return currency;
  }

  return {}
};
