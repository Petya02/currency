import React from "react";
import { Row, Col, Form, Button } from "react-bootstrap";

const Translate = (prop) => {
  return (
    <>
      <Row className="g-2" style={{ alignItems: "center", width: "50%", border: "1px solid #ccc", borderRadius: "10px", padding: "50px", marginLeft: "auto", marginRight: "auto", marginTop: "40px" }}>
        <Col md>
          <Form.Control type="text" placeholder="15 usd in rub" value={prop.value} onChange={(e) => prop.onChange(e.target.value)} />
        </Col>
        <Button onClick={prop.onClick} style={{width: "30%"}} variant="dark">Convert</Button>
      </Row>
      <Row className="g-2" style={{ alignItems: "center", width: "50%", border: "1px solid #ccc", borderRadius: "10px", padding: "50px", marginLeft: "auto", marginRight: "auto", marginTop: "40px" }}>
        Result: {prop.result ? prop.result : "0 RUB"}
      </Row>
    </>
  )
};

export default Translate;