import React from "react";
import { Link } from "react-router-dom";
import { Navbar, Nav, Container } from "react-bootstrap"

const Header = (props) => {
  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand href="#">Navbar</Navbar.Brand>
        <Nav className="ml-auto">
          <Link to={props.onHome} style={{ color: "#ccc", textDecoration: "none", marginRight: "50px" }}>Home</Link>
          <Link to={props.onCurrency} style={{ color: "#ccc", textDecoration: "none" }}>Currency</Link>
        </Nav>
      </Container>
    </Navbar>
  )
};

export default Header;