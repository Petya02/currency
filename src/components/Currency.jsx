import React from "react";
import { Card, Tabs, Tab } from "react-bootstrap";

const Currency = (prop) => {
  const usd = Object.entries(prop.valuet.usd)
  const rub = Object.entries(prop.valuet.rub)
  const eur = Object.entries(prop.valuet.eur)

  if(prop.USD && prop.RUB && prop.EUR){
    return (
      <div class="d-flex justify-content-center">
        <div class="spinner-border" role="status">
        </div>
      </div>
    )
  }

  console.log(prop.valuet.usd);

  return (<>
    <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="mb-3">
      <Tab eventKey="USD" title="USD">
        <Card style={{ flexDiraction: "row", flexWrap: "wrap" }}>
          {usd.map((item, i) => {
            return <Card.Body style={{border: "1px solid #ccc", width: "200px", margin: "10px 20px"}} key={i}>{item[0]}/USD - {item[1]}</Card.Body>
          })}
        </Card>
      </Tab>
      <Tab eventKey="RUB" title="RUB">
        <Card style={{ flexDiraction: "row", flexWrap: "wrap" }}>
          {rub.map((item, i) => {
            return <Card.Body style={{border: "1px solid #ccc", width: "200px", margin: "10px 20px"}} key={i}>{item[0]}/RUB - {item[1]}</Card.Body>
          })}
        </Card>
      </Tab>
      <Tab eventKey="EUR" onC title="EUR">
        <Card style={{ flexDiraction: "row", flexWrap: "wrap" }}>
          {eur.map((item, i) => {
            return <Card.Body style={{border: "1px solid #ccc", width: "200px", margin: "10px 20px"}} key={i}>{item[0]}/EUR - {item[1]}</Card.Body>
          })}
        </Card>
      </Tab>
    </Tabs>
  </>
  )
};

export default Currency;