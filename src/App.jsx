import React from "react";
import "./App.css";
import axios from 'axios';
import Currency from "./components/Currency";
import Header from "./components/Header";
import Translate from "./components/Translate";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Valuet } from "./store/currency";

function App() {
  const [value, setValue] = React.useState('')
  const [res, setRes] = React.useState('0')
  const [resValue, setResValue] = React.useState('')

  const valuet = Valuet();

  console.log(valuet);

  const Result = (value) => {
    let prise = value.split(' ')[0];
    let firstValiut = value.split(' ')[1].toUpperCase();
    let secondValiut = value.split(' ')[3].toUpperCase();
    setResValue(secondValiut)

    const getCurrentPrice = (count, valut) => {
      return count * valut['conversion_rate'];
    }

    axios({
      method: "GET",
      url: `https://v6.exchangerate-api.com/v6/cfb6d4cb0ddf0ee3419336f1/pair/${firstValiut}/${secondValiut}`,
    })
      .then((response) => {
        setRes(`${Math.round(getCurrentPrice(prise, response.data))}`)
      })
      .catch((error) => {
        console.log("axios error:", error);
      });

  }

  return (
    <BrowserRouter className="App">
      <Header
        onHome={"/"}
        onCurrency={"/translate"}
      />
      <Switch>
        <Route path={"/translate"} >
          <Translate
            value={value}
            onChange={setValue}
            result={`${res} ${resValue}`}
            onClick={() => Result(value)}
          />
        </Route>
        <Route path={"/"} >
          <Currency 
            valuet={valuet}
          />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
